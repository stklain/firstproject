﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventExample
{
    class FuelEventArgs:EventArgs
    {
        public readonly int Fuel;
        public FuelEventArgs( int fuel)
        {
            Fuel = fuel;
        }
    }
}
